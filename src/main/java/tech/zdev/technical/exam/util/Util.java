package tech.zdev.technical.exam.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import tech.zdev.technical.exam.model.Recommendation;

public class Util {

	public static List<Recommendation> getRecomendationsOfFile(String file) {
		JsonReader flights = null;
		try {
			System.out.println(file);
			flights = new JsonReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		Type recommendationsType = new TypeToken<ArrayList<Recommendation>>() {
		}.getType();
		return new Gson().fromJson(flights, recommendationsType);
	}

	public static int[] getListNumbersOfFile(String file) {
		int[] listNumbers = {};
		String[] arr = {};
		try (Stream<String> stream = Files.lines(Paths.get(file))) {
			arr = stream.toArray(String[]::new);
			listNumbers = Stream.of(arr).mapToInt(Integer::parseInt).toArray();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return listNumbers;
	}

}
