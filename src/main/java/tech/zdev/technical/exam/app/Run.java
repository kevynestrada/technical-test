package tech.zdev.technical.exam.app;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import tech.zdev.technical.exam.controller.ProblemaDos;
import tech.zdev.technical.exam.controller.ProblemaUno;
import tech.zdev.technical.exam.model.Recommendation;
import tech.zdev.technical.exam.util.Util;

public class Run {

	public static void main(String[] args) {
		if (valideArgs(args)) {
			System.exit(0);
		} else {
			System.exit(1);
		}

	}

	private static boolean valideArgs(String[] args) {
		if (args.length != 2) {
			System.out.println("Se debe especificar 2 parametros");
			return false;
		}
		if (!Files.exists(Paths.get(args[1]))) {
			System.out.println(args[1] + " No se ha podido abrir.");
			return false;
		}
		switch (args[0]) {
		case "problema1":
			int[] list = Util.getListNumbersOfFile(args[1]);
			ProblemaUno problemaUno = new ProblemaUno();
			System.out.println("Input :");
			System.out.println(Arrays.toString(list));
			System.out.println("Results :");
			System.out.println(Arrays.toString(problemaUno.getTheInvertedArray(list)));
			break;
		case "problema2":
			List<Recommendation> recommendations = Util.getRecomendationsOfFile(Paths.get(args[1]).toAbsolutePath().toString());
			ProblemaDos problemaDos = new ProblemaDos();
			System.out.println("Input :");
			Gson gson = new Gson();
			System.out.println(gson.toJson(recommendations));
			System.out.println("Results (vuelos directos):");
			System.out.println(gson.toJson(problemaDos.filterFlights(recommendations, ProblemaDos.isDirectFlight())));
			System.out.println("Results (incluyen equipaje):");
			System.out.println(gson.toJson(problemaDos.filterFlights(recommendations, ProblemaDos.isIncludeBaggage())));
			System.out.println("Results (precio maximo 800):");
			System.out.println(gson.toJson(problemaDos.filterFlights(recommendations, ProblemaDos.isMaximumPriceTotal(800))));
			break;
		default:
			System.out.println("Debe escojer entre problema1 o problema2");
			break;
		}
		return true;
	}

}
