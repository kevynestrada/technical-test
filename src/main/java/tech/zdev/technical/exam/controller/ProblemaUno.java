package tech.zdev.technical.exam.controller;

public class ProblemaUno {

    public int[] getTheInvertedArray(int[] numbers) {
    	int rightNumber;
    	int rightIndex;
    	int totalNumbers = numbers.length;
    	for (int i = 0; i < totalNumbers/2; i++) {
    		rightIndex = (totalNumbers - 1) - i;
    		rightNumber = numbers[rightIndex];
    		numbers[rightIndex] = numbers[i];
    		numbers[i] = rightNumber;
		}
    	return numbers;
    }

}
