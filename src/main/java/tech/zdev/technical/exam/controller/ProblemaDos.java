package tech.zdev.technical.exam.controller;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import tech.zdev.technical.exam.model.Recommendation;

public class ProblemaDos {

	public static Predicate<Recommendation> isDirectFlight() {
		return recommendation -> recommendation.getItinerary().get(0).getFlights().stream()
				.allMatch(s -> s.getSegments().size() == 1);
	}

	public static Predicate<Recommendation> isIncludeBaggage() {
		return recommendation -> recommendation.getItinerary().get(0).getFlights().stream()
				.allMatch(s -> s.getBaggage().getPieces() > 0);
	}

	public static Predicate<Recommendation> isMaximumPriceTotal(float price) {
		return recommendation -> recommendation.getPricing().getTotal() <= price;
	}

	public List<Recommendation> filterFlights(List<Recommendation> recommendations, Predicate<Recommendation> filter) {
		return recommendations.stream().filter(filter).collect(Collectors.toList());
	}

}
