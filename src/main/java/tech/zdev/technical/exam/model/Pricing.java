package tech.zdev.technical.exam.model;

import java.util.List;

public class Pricing {

	private String base;
	private List<Object> discounts;
	private String fareQualifier;
	private List<Object> fees;
	private Passenger passenger;
	private String source;
	private float taxes;
	private float total;
	private float totalAmount;
	private String validatingAirline;

	public String getBase() {
		return this.base;
	}

	public List<Object> getDiscounts() {
		return this.discounts;
	}

	public String getFareQualifier() {
		return this.fareQualifier;
	}

	public List<Object> getFees() {
		return this.fees;
	}

	public Passenger getPassengers() {
		return this.passenger;
	}

	public String getSource() {
		return this.source;
	}

	public float getTaxes() {
		return taxes;
	}

	public float getTotal() {
		return this.total;
	}

	public float getTotalAmount() {
		return this.totalAmount;
	}

	public String getValidatingAirline() {
		return this.validatingAirline;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public void setDiscounts(List<Object> discounts) {
		this.discounts = discounts;
	}

	public void setFareQualifier(String fareQualifier) {
		this.fareQualifier = fareQualifier;
	}

	public void setFees(List<Object> fees) {
		this.fees = fees;
	}

	public void setPassengers(Passenger passengersObject) {
		this.passenger = passengersObject;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTaxes(float taxes) {
		this.taxes = taxes;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setValidatingAirline(String validatingAirline) {
		this.validatingAirline = validatingAirline;
	}
}
