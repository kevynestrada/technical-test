package tech.zdev.technical.exam.model;

public class MarketingAirline {

	private String code;
	private String name;

	public String getCode() {
		return this.code;
	}

	public String getName() {
		return this.name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}
}
