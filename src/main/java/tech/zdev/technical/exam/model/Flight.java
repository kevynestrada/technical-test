package tech.zdev.technical.exam.model;

import java.util.List;

public class Flight {

	private ArrivalAirport arrivalAirport;
	private String arrivalDateTime;
	private Baggage baggage;
	private DepartureAirport departureAirport;
	private String departureDateTime;
	private String elapsedTime;
	private String id;
	private float itineraryReference;
	private MarketingAirline marketingAirline;
	private String rph;
	private List<Object> segments;

	public ArrivalAirport getArrivalAirport() {
		return arrivalAirport;
	}

	public String getArrivalDateTime() {
		return arrivalDateTime;
	}

	public Baggage getBaggage() {
		return baggage;
	}

	public DepartureAirport getDepartureAirport() {
		return departureAirport;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public String getElapsedTime() {
		return elapsedTime;
	}

	public String getId() {
		return id;
	}

	public float getItineraryReference() {
		return itineraryReference;
	}

	public MarketingAirline getMarketingAirline() {
		return marketingAirline;
	}

	public String getRph() {
		return rph;
	}

	public List<Object> getSegments() {
		return segments;
	}

	public void setArrivalAirport(ArrivalAirport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}

	public void setDepartureAirport(DepartureAirport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setItineraryReference(float itineraryReference) {
		this.itineraryReference = itineraryReference;
	}

	public void setMarketingAirline(MarketingAirline marketingAirline) {
		this.marketingAirline = marketingAirline;
	}

	public void setRph(String rph) {
		this.rph = rph;
	}

	public void setSegments(List<Object> segments) {
		this.segments = segments;
	}
}
