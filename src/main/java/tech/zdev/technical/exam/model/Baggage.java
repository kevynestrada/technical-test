package tech.zdev.technical.exam.model;

public class Baggage {

	private int pieces;

	public int getPieces() {
		return this.pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}
}
