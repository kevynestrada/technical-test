package tech.zdev.technical.exam.model;

import java.util.List;

public class Recommendation {

	private String id;
	private List<Itinerary> itinerary;
	private Pos pos;
	private Pricing pricing;

	public String getId() {
		return this.id;
	}

	public List<Itinerary> getItinerary() {
		return itinerary;
	}

	public Pos getPos() {
		return this.pos;
	}

	public Pricing getPricing() {
		return this.pricing;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setItinerary(List<Itinerary> itinerary) {
		this.itinerary = itinerary;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public void setPricing(Pricing pricing) {
		this.pricing = pricing;
	}

}