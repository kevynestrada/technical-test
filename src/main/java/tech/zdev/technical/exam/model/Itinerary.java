package tech.zdev.technical.exam.model;

import java.util.List;

public class Itinerary {

	private List<Flight> flights;

	public List<Flight> getFlights() {
		return this.flights;
	}

	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}
}