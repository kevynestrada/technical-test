package tech.zdev.technical.exam.model;

public class Adults {

	private String base;
	private String contextCode;
	private String taxes;
	private String total;

	public String getBase() {
		return base;
	}

	public String getContextCode() {
		return contextCode;
	}

	public String getTaxes() {
		return taxes;
	}

	public String getTotal() {
		return total;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public void setContextCode(String contextCode) {
		this.contextCode = contextCode;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
