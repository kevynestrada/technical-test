package tech.zdev.technical.exam.model;

public class DepartureAirport {

	private String code;
	private String description;

	public String getCode() {
		return this.code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
