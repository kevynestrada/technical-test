package tech.zdev.technical.exam.model;

public class Passenger {
	private Adults adults;

	public Adults getAdults() {
		return this.adults;
	}

	public void setAdults(Adults adultsObject) {
		this.adults = adultsObject;
	}
}
