package tech.zdev.technical.exam.model;

public class Pos {

	private String officeId;
	private int systemProviderCode;

	public String getOfficeId() {
		return this.officeId;
	}

	public int getSystemProviderCode() {
		return this.systemProviderCode;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public void setSystemProviderCode(int systemProviderCode) {
		this.systemProviderCode = systemProviderCode;
	}
}
