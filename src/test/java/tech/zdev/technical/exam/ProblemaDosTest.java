package tech.zdev.technical.exam;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import tech.zdev.technical.exam.controller.ProblemaDos;
import tech.zdev.technical.exam.model.Recommendation;
import tech.zdev.technical.exam.util.Util;

public class ProblemaDosTest {

	private ProblemaDos problemaDos;

	@Before
	public void setUp() {
		problemaDos = new ProblemaDos();
	}

	@Test
	public void deberiaObtenerSoloLosVuelosDirectos() {

		List<Recommendation> recommendations = getRecomendations();

		// En este metodo se deben de agregar los parametros del filtro
		List<Recommendation> filterRecommendations = problemaDos.filterFlights(recommendations,
				ProblemaDos.isDirectFlight());

		assertTrue(filterRecommendations.size() == 3);

	}

	@Test
	public void deberiaObtenerSoloLosVuelosQueIncluyanEquipaje() {

		List<Recommendation> recommendations = getRecomendations();

		// En este metodo se deben de agregar los parametros del filtro
		List<Recommendation> filterRecommendations = problemaDos.filterFlights(recommendations,
				ProblemaDos.isIncludeBaggage());

		assertTrue(filterRecommendations.size() == 5);

	}

	@Test
	public void deberiaObtenerSoloLosVuelosDeUnPrecioMaximoDe800() {

		List<Recommendation> recommendations = getRecomendations();

		// En este metodo se deben de agregar los parametros del filtro
		// Precio Maximo $800
		List<Recommendation> filterRecommendations = problemaDos.filterFlights(recommendations,
				ProblemaDos.isMaximumPriceTotal(800));

		assertTrue(filterRecommendations.size() == 1);

	}

	private List<Recommendation> getRecomendations() {
		// En este metodo debes de leer las recomendaciones
		// del archivo flights.json que lo encuentras en
		// la carpeta resources. Debes de hacer un parse
		// de json a objetos
		return Util.getRecomendationsOfFile(getClass().getClassLoader().getResource("flights.json").getFile());
	}

}
