package tech.zdev.technical.exam;

import org.junit.Before;
import org.junit.Test;

import tech.zdev.technical.exam.controller.ProblemaUno;

import static org.junit.Assert.assertTrue;

public class ProblemaUnoTest {

    private ProblemaUno problemaUno;

    @Before
    public void setUp() {
        problemaUno = new ProblemaUno();
    }

    @Test
    public void retornarElArrayInvertidoDeCuatroNumero() {
        int numbers[] = {20, 10, 30, 40};

        int[] arrayInverted = problemaUno.getTheInvertedArray(numbers);

        assertTrue(arrayInverted[0] == 40 && arrayInverted[1] == 30 && arrayInverted[2] == 10 && arrayInverted[3] == 20);
    }
    
    @Test
    public void retornarElArrayInvertidoDeCincoNumeros() {
        int numbers[] = {20, 10, 50, 30, 40};

        int[] arrayInverted = problemaUno.getTheInvertedArray(numbers);

        assertTrue(arrayInverted[0] == 40 && arrayInverted[1] == 30 && arrayInverted[2] == 50 && arrayInverted[3] == 10 && arrayInverted[4] == 20);
    }
    
    @Test
    public void retornarElArrayInvertidoDeCeroNumeros() {
        int numbers[] = {};

        int[] arrayInverted = problemaUno.getTheInvertedArray(numbers);

        assertTrue(arrayInverted.length == 0);
    }
    
}
