
# technical-test
Prueba Técnica para Costamar Travel

## Build
Ejemplo:
> mvn clean compile assembly:single

## Run
Ejemplo:
> java -jar target/technical-test-1.0-jar-with-dependencies.jar problema1 numeros.txt
> java -jar target/technical-test-1.0-jar-with-dependencies.jar problema2 flights.json